//
//  SearchData.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import Foundation

struct ServerData : Codable {
    var data : ProductsData?
    var message : String?
}
