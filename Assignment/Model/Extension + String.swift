//
//  Extension + String.swift
//  Assignment
//
//  Created by Tom Pruimboom on 26/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit

extension String {

    func changePriceToSellingPrice(price: String) -> String {
        if price.contains(".0" ) {
            return NSLocalizedString((price.replacingOccurrences(of: ".0", with: ",-")), comment: "")
        }
        else {
            return NSLocalizedString("\(price)" , comment: "")
        }
    }

    func createCorrectUrlString(urlString: String) -> String {
        let correctUrl = "https:\(urlString)"
        return correctUrl
    }
}
