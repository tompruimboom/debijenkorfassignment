//
//  ShownImage.swift
//  Assignment
//
//  Created by Tom Pruimboom on 23/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

struct ShownImage : Codable {
    var url: String?
}
