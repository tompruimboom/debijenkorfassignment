//
//  SellingPrice.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

struct SellingPrice : Codable {
    var value : Float?
}
