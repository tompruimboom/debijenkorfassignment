//
//  ApiService.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import Foundation
import UIKit

class ApiService {

    static let shared = ApiService()

    func getServerDataFromSearch(searchString: String, completion: @escaping (ServerData?, Error?) -> Void) {

        let formattedString = searchString.replacingOccurrences(of: " ", with: "%20")
        let urlString = "https://ceres-catalog.debijenkorf.nl/catalog/navigation/search?text=\(formattedString)"
        guard let url = URL(string : urlString) else { return }

        let task = URLSession.shared.dataTask(with: url) { (data, response, error) in
            if error != nil {
                completion(nil, error)
            }
            guard let data = data else { return }
            let decoder = JSONDecoder()
            do {
                let dataList = try decoder.decode(ServerData.self , from: data)
                completion(dataList, nil)
            }
            catch {
                completion(nil, error)
            }
        }
        task.resume()
    }
}

