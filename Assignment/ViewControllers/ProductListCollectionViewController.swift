//
//  ProductListCollectionViewController.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

class ProductListCollectionViewController: UICollectionViewController {

    var searchText = String()
    var detailSegueID = "detailSegue"
    var headerIdentifier = "header"
    var productCellIdentifier = "productCell"
    var listViewModel : ProductsViewModel?
    var detailViewModel : DetailViewViewModel?

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup() {
        setTitleView()
        setCollectionView()
        listViewModel = ProductsViewModel(searchText: searchText, delegate: self)
    }

    func setTitleView(){
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named:"deBijenKorfLogo")
        navigationItem.titleView = imageView
        navigationItem.titleView?.sizeToFit()
    }

    func setCollectionView(){
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            layout.sectionHeadersPinToVisibleBounds = true
            collectionView.collectionViewLayout = layout
        }
    }

    override func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {

        switch kind {
        case UICollectionView.elementKindSectionHeader:
            let headerView = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionView.elementKindSectionHeader, withReuseIdentifier: headerIdentifier, for: indexPath) as! HeaderCollectionViewCell
            headerView.setCell(searchText: searchText)

            return headerView

        default:  fatalError()
        }
    }

    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let amountOfItems = listViewModel?.products?.count else { return 0}
        return amountOfItems
    }

    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let productCell = collectionView.dequeueReusableCell(withReuseIdentifier: productCellIdentifier, for: indexPath) as? ProductCollectionViewCell else { preconditionFailure()
        }
        guard let product = listViewModel?.products?[indexPath.item] else { return productCell }
        productCell.setCell(product: product)
        return productCell
    }

    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let selectedIndex = indexPath.item
        guard let pushedProduct = listViewModel?.products?[selectedIndex] else { return }
        detailViewModel = DetailViewViewModel(product: pushedProduct)
        performSegue(withIdentifier: detailSegueID, sender: self)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailSegueID {
            let destinationController = segue.destination as? DetailViewController
            destinationController?.detailViewModel = detailViewModel
        }
    }

    func showNoConnectionwithServerAlert(){
        let title = NSLocalizedString("Server error", comment: "")
        let message = NSLocalizedString("Er ging iets fout", comment: "")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true)
        navigationController?.popViewController(animated: true)
    }

    func noProductsFoundAlert() {
        let title = NSLocalizedString("Niets gevonden", comment: "")
        let message = NSLocalizedString("We kunnen helaas geen producten vinden met uw zoekopdracht", comment: "")
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: "Ok", style: .cancel) { (action) in
            self.navigationController?.popViewController(animated: true)
        }
        alert.addAction(action)
        self.present(alert, animated: true)
        navigationController?.popViewController(animated: true)
    }
}

extension ProductListCollectionViewController : ReloadProtocol {
    func noProductsFound() {
        noProductsFoundAlert()
    }

    func notifyNoServerInfo() {
        showNoConnectionwithServerAlert()
    }

    func reloadData() {
        collectionView.reloadData()
    }
}
