//
//  DetailViewController.swift
//  Assignment
//
//  Created by Tom Pruimboom on 23/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit
import Kingfisher

class DetailViewController: UIViewController, UIScrollViewDelegate {

    var detailViewModel : DetailViewViewModel?
    
    @IBOutlet weak var inCartButton: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var scrollView: UIScrollView!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    func setup(){

        navigationItem.title = detailViewModel?.brand
        
        guard let urlStringWithoutHttps = detailViewModel?.imageString else { return }
        let urlString = urlStringWithoutHttps.createCorrectUrlString(urlString: urlStringWithoutHttps)

        let url = URL(string: urlString)
        productImageView.kf.setImage(with: url)
        descriptionLabel.text = detailViewModel?.name

        guard let priceFloat = detailViewModel?.price else { return }
        let priceString = String(describing: priceFloat)

        priceLabel.text = priceString.changePriceToSellingPrice(price: priceString)

        setInCartButton()
    }

    func setInCartButton(){
        inCartButton.setTitle(NSLocalizedString("In winkelwagen", comment: ""), for: .normal)
        inCartButton.layer.borderWidth = 1
        inCartButton.layer.borderColor = UIColor.black.cgColor
    }
}
