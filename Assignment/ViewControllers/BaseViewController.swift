//
//  ViewController.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    var searchText = ""
    var data: ServerData?
    var productListSegueID = "openProductListSegue"
    var buttonColor = UIColor(red:53/255, green: 92/255, blue: 125/255, alpha: 1.0)

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var searchButton: UIButton!
    @IBOutlet weak var label: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        setup()
    }

    @IBAction func searchButtonPressed(_ sender: UIButton) {
        guard let text = textField.text else { return }
        searchText = text
        performSegue(withIdentifier: productListSegueID, sender: self)
    }

    func setup() {
        setSearchButtonUI()
        setTextField()
        label.text = NSLocalizedString("Doorzoek ons assortiment", comment: "")
    }

    func setSearchButtonUI(){
        searchButton.isEnabled = false
        searchButton.layer.borderWidth = 1
        searchButton.layer.borderColor = UIColor.black.cgColor
        searchButton.layer.backgroundColor = UIColor.lightGray.cgColor
    }

    func setTextField(){
        textField.delegate = self
        textField.becomeFirstResponder()
        textField.placeholder = NSLocalizedString("Zoek", comment: "")
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == productListSegueID {
            let destinationController = segue.destination as? ProductListCollectionViewController
            destinationController?.searchText = self.searchText
        }
    }
}

extension BaseViewController : UITextFieldDelegate {

    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {

        guard let text = (textField.text as? NSString)?.replacingCharacters(in: range, with: string) else { return false }
        if !text.isEmpty {
            searchButton.isEnabled = true
            searchButton.layer.backgroundColor = buttonColor.cgColor
        } else {
            setSearchButtonUI()
        }
        return true
    }
}

