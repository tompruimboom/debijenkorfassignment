//
//  ProductCollectionViewCell.swift
//  Assignment
//
//  Created by Tom Pruimboom on 23/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit
import Kingfisher

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var brandNameLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var imageWithConstraint: NSLayoutConstraint!

    func setCell(product: Product) {

        imageWithConstraint.constant = CGFloat(UIScreen.main.bounds.width / 2 - 30)

        guard let imageUrlString = product.currentVariantProduct?.images?.first?.url, let name = product.name, let price = product.sellingPrice?.value, let displayedProductName = product.brand?.name else { return }

        let priceDescriptionString = String(describing: price)
        let priceString = priceDescriptionString.changePriceToSellingPrice(price: priceDescriptionString)

        brandNameLabel.text = displayedProductName
        nameLabel.text = name
        priceLabel.text = priceString

        let urlstring = imageUrlString.createCorrectUrlString(urlString: imageUrlString)
        let url = URL(string: urlstring)
        productImageView.kf.setImage(with: url)

        self.layer.borderWidth = 1
        self.layer.borderColor = UIColor.lightGray.cgColor
        self.layer.cornerRadius = 5
    }
}
