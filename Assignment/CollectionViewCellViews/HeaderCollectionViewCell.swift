//
//  HeaderCollectionViewCell.swift
//  Assignment
//
//  Created by Tom Pruimboom on 22/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

class HeaderCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var searchResultLabel: UILabel!

    func setCell(searchText: String) {
        searchResultLabel.text = NSLocalizedString("U heeft gezocht op '\(searchText)'", comment: "")
    }
}
