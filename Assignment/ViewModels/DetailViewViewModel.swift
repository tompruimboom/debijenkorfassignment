//
//  DetailViewViewModel.swift
//  Assignment
//
//  Created by Tom Pruimboom on 24/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import Foundation

class DetailViewViewModel {

    var brand = String()
    var name =  String()
    var price = Float()
    var imageString = String()

    init(product: Product){
        guard let brandName = product.brand?.name, let name = product.name, let price = product.sellingPrice?.value, let productImageString = product.currentVariantProduct?.images?.first?.url else { return }

        self.brand = brandName
        self.name = name
        self.price = price
        self.imageString = productImageString
    }
}
