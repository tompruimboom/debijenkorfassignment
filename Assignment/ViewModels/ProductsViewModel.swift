//
//  ProductsViewModel.swift
//  Assignment
//
//  Created by Tom Pruimboom on 24/01/2020.
//  Copyright © 2020 Tom Pruimboom. All rights reserved.
//

import UIKit

protocol ReloadProtocol {
    func reloadData()
    func notifyNoServerInfo()
    func noProductsFound()
}

class ProductsViewModel {

    var products : [Product]?
    var delegate : ReloadProtocol?

    required init(searchText: String, delegate: ReloadProtocol){
        getSearchedProducts(searchText: searchText)
        self.delegate = delegate
    }

    func getSearchedProducts(searchText: String) {
        ApiService.shared.getServerDataFromSearch(searchString: searchText) { (searchData, error) in
            if error != nil {
                self.notifyNoServerInfo()
            }
            guard let data = searchData  else { return }
            DispatchQueue.main.async {
                if data.data?.products == nil {
                    self.noProductsFound()
                }
                else {
                    self.products = data.data?.products
                    self.reloadData()
                }
            }
        }
    }
}

extension ProductsViewModel : ReloadProtocol {
    func noProductsFound() {
        delegate?.noProductsFound()
    }


    func reloadData() {
        delegate?.reloadData()
    }

    func notifyNoServerInfo() {
        delegate?.notifyNoServerInfo()
    }
}
